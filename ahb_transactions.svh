`ifndef AHB_TRANSACTIONS_SVH
`define AHB_TRANSACTIONS_SVH
class ahb_transaction#(
	ADDR_WIDTH=32,
	DATA_WIDTH=32,
	MAX_BEATS=1024
) extends uvm_sequence_item;

	`uvm_object_utils(ahb_transaction)

	// Transaction
	rand logic	[ADDR_WIDTH-1:0]		base_addr;

	rand logic	[MAX_BEATS-1:0][ADDR_WIDTH-1:0]	HADDR;
	rand hburst					HBURST;
	rand logic	[2:0]				HSIZE;
//	rand htrans					HTRANS;
	rand logic					HWRITE;
	rand logic	[MAX_BEATS-1:0][DATA_WIDTH-1:0]	data;
	rand logic	[$clog2(MAX_BEATS)-1:0]		valid_index;	// For INCR bursts

	constraint base_align {
		(base_addr % (DATA_WIDTH/8)) == 0;
	};

	constraint size_limit {
		if(DATA_WIDTH >= 1024) {
			HSIZE <= 3'b111;
		} else if(DATA_WIDTH >= 512) {
			HSIZE <= 3'b110;
		} else if(DATA_WIDTH >= 256) {
			HSIZE <= 3'b101;
		} else if(DATA_WIDTH >= 128) {
			HSIZE <= 3'b100;
		} else if(DATA_WIDTH >= 64) {
			HSIZE <= 3'b011;
		} else if(DATA_WIDTH >= 32) {
			HSIZE <= 3'b010;
		} else if(DATA_WIDTH >= 16) {
			HSIZE <= 3'b001;
		} else if(DATA_WIDTH >= 8) {
			HSIZE <= 3'b000;
		} else {
			HSIZE <= 3'b000;
		}
	};
/*
	constraint addr_incr {
		//if(	INCR == HBURST || INCR4 == HBURST || INCR8 == HBURST || INCR16 == HBURST) {
		if(SINGLE != HBURST) {
		// TODO: Not worrying about 1KB address boundary right now
			foreach (HADDR[i]) {
				HADDR[i] == (base_addr + i*(DATA_WIDTH/8));
			}
		}
	};
*/

	constraint incr_addr {
			HADDR[0] == base_addr;
			HADDR[1] == (base_addr + (DATA_WIDTH/8));
			HADDR[2] == (base_addr + 2*(DATA_WIDTH/8));
			HADDR[3] == (base_addr + 3*(DATA_WIDTH/8));
			HADDR[4] == (base_addr + 4*(DATA_WIDTH/8));
			HADDR[5] == (base_addr + 5*(DATA_WIDTH/8));
			HADDR[6] == (base_addr + 6*(DATA_WIDTH/8));
			HADDR[7] == (base_addr + 7*(DATA_WIDTH/8));
			HADDR[8] == (base_addr + 8*(DATA_WIDTH/8));
			HADDR[9] == (base_addr + 9*(DATA_WIDTH/8));
			HADDR[10] == (base_addr + 10*(DATA_WIDTH/8));
			HADDR[11] == (base_addr + 11*(DATA_WIDTH/8));
			HADDR[12] == (base_addr + 12*(DATA_WIDTH/8));
			HADDR[13] == (base_addr + 13*(DATA_WIDTH/8));
			HADDR[14] == (base_addr + 14*(DATA_WIDTH/8));
			HADDR[15] == (base_addr + 15*(DATA_WIDTH/8));	
	}

	function new(string name="ahb_transaction");
		super.new(name);
	endfunction: new

	function string convert2string();
		string s = super.convert2string();
		s = {s, $sformatf("\n HADDR: %0h, HBURST: %0h, HSIZE: %0h, HWRITE: %0h", HADDR, HBURST, HSIZE)};
		return s;
	endfunction: convert2string

endclass: ahb_transaction
`endif
