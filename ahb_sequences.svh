`ifndef AHB_SEQUENCES_SVH
`define AHB_SEQUENCES_SVH

class ahb_sequence extends uvm_sequence#(ahb_transaction);

	`uvm_object_utils(ahb_sequence)

	//int unsigned count = 5000; // Knob for # of transactions

	function new(string name="");
		super.new(name);
	endfunction: new

	task body();
		ahb_transaction trans; // Create transaction handle

		//trans = ahb_transaction::type_id::create("trans", .contxt(get_full_name()));
		trans = new();
		start_item(trans);
		finish_item(trans);
	endtask: body

endclass: ahb_sequence

class ahb_rand_sequence extends uvm_sequence#(ahb_transaction);

	`uvm_object_utils(ahb_rand_sequence)

	int unsigned count = 5000; // Knob for # of transactions

	function new(string name="");
		super.new(name);
	endfunction: new

	task body();
		ahb_transaction trans; // Create transaction handle

		repeat(count) begin: tx_create_loop // Does this create a memory leak?
			trans = ahb_transaction#(32,32,1024)::type_id::create("trans", .contxt(get_full_name()));
			start_item(trans);
			if(!trans.randomize()) begin
				`uvm_error(get_full_name(), "Failed to randomize transaction")
			end
			finish_item(trans);
		end: tx_create_loop
	endtask: body

endclass: ahb_rand_sequence

/*
class apb_sequence_library extends uvm_sequence_library#(apb_transaction);

	`uvm_object_utils(apb_sequence_library)

	function new(string name="apb_sequence_library");
		super.new(name);
        
        add_typewide_sequences({apb_write::get_type(),apb_read::get_type()});
        init_sequence_library();
	endfunction: new

endclass: apb_sequence_library
*/

`endif
