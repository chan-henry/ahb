`include "ahb_if.sv"
`include "ahb_master_driver_bfm.sv"

`timescale 1ns/1ns

module tb;

	import uvm_pkg::*;

	logic clk = 0;
	logic rst = 0;

	ahb_if ahb_if_inst(clk, rst);

	ahb_master_driver_bfm bfm(ahb_if_inst);

	// Clk gen
	initial begin
		`uvm_info("tb", "Generating clk", UVM_LOW)
		forever #5 clk = ~clk;
	end

	// Rst
	initial begin
		`uvm_info("tb", "Generating rst", UVM_LOW)
		#200;
		@(posedge clk);
		rst = 1;
		//#100_000ns;
		//$finish();
	end

	initial begin
		uvm_config_db#(virtual ahb_master_driver_bfm)::set(.cntxt(null), .inst_name("uvm_test_top.*"), .field_name("bfm"), .value(bfm));
		`uvm_info("tb", "Set ahb_master_driver_bfm into uvm_config_db", UVM_LOW)
		run_test();
	end

endmodule
