`ifndef AHB_TYPES_SVH
`define AHB_TYPES_SVH

typedef enum bit[1:0] {
	IDLE	= 2'b00,
	BUSY	= 2'b01,
	NONSEQ	= 2'b10,
	SEQ	= 2'b11
} htrans;

typedef enum bit[2:0] {
	SINGLE	= 3'b000,	// Single transfer burst
	INCR	= 3'b001,	// Incrementing burst of undefined length
	WRAP4	= 3'b010,	// 4-beat wrapping burst
	INCR4	= 3'b011,	// 4-beat incrementing burst
	WRAP8	= 3'b100,	// 8-beat wrapping burst
	INCR8	= 3'b101,	// 8-beat incrementing burst
	WRAP16	= 3'b110,	// 16-beat wrapping burst
	INCR16	= 3'b111	// 16-beat incrementing burst
} hburst;

`endif // def AHB_TYPES_SVH
