`ifndef AHB_IF_SV
`define AHB_IF_SV
interface ahb_if#(
	ADDR_WIDTH=32,
	DATA_WIDTH=32
)
(
	input logic HCLK,
	input logic HRESETn
);

	import ahb_pkg::*;

	logic	[ADDR_WIDTH-1:0]	HADDR;
	//logic	[2:0]			HBURST;
	hburst				HBURST;
	logic				HMASTLOCK;
	logic	[6:0]			HPROT;
	logic	[2:0]			HSIZE;
	logic				HNONSEC;
	logic				HEXCL;
	logic	[3:0]			HMASTER;
	//logic	[1:0]			HTRANS;
	htrans				HTRANS;
	logic	[DATA_WIDTH-1:0]	HWDATA;
	logic				HWRITE;

	logic	[DATA_WIDTH-1:0]	HRDATA;
	logic				HREADYOUT;
	logic				HRESP;
	logic				HEXOKAY;
	logic				HSEL;

	logic				HREADY = 1; // FIXME: This is temporary until slave is attached

	clocking master_cb @(posedge HCLK);
		//input	HRESETn;
		input	HREADY;
		input	HRESP;
		input	HRDATA;

		output	HADDR;
		output	HWRITE;
		output	HSIZE;
		output	HBURST;
		output	HPROT;
		output	HTRANS;
		output	HMASTLOCK;
		output	HWDATA;
	endclocking

	clocking slave_cb @(posedge HCLK);
		//input	HRESETn;
		input	HADDR;
		input	HWRITE;
		input	HSIZE;
		input	HBURST;
		input	HPROT;
		input	HTRANS;
		input	HMASTLOCK;
		input	HWDATA;

		output	HREADYOUT;
		output	HRESP;
		output	HRDATA;
	endclocking

endinterface
`endif
