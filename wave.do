onerror { resume }
set curr_transcript [transcript]
transcript off

add wave -expand /tb/bfm/trans
add wave /tb/bfm/addr_index
add wave /tb/bfm/data_index
add wave -logic /tb/bfm/trans_toggle0
add wave -logic /tb/bfm/trans_toggle1
add wave -logic /tb/bfm/trans_toggle2
add wave /tb/bfm/write_wdata
add wave /tb/bfm/read_rdata
add wave /tb/bfm/send_rsp_trans
add wave /tb/bfm/execute_trans.proxy_trans
add wave /tb/ahb_if_inst/HCLK
add wave /tb/ahb_if_inst/HRESETn
add wave /tb/ahb_if_inst/HADDR
add wave /tb/ahb_if_inst/HBURST
add wave /tb/ahb_if_inst/HMASTLOCK
add wave /tb/ahb_if_inst/HPROT
add wave /tb/ahb_if_inst/HSIZE
add wave /tb/ahb_if_inst/HNONSEC
add wave /tb/ahb_if_inst/HEXCL
add wave /tb/ahb_if_inst/HMASTER
add wave /tb/ahb_if_inst/HTRANS
add wave /tb/ahb_if_inst/HWDATA
add wave /tb/ahb_if_inst/HWRITE
add wave /tb/ahb_if_inst/HRDATA
add wave /tb/ahb_if_inst/HREADYOUT
add wave /tb/ahb_if_inst/HRESP
add wave /tb/ahb_if_inst/HEXOKAY
add wave /tb/ahb_if_inst/HSEL
add wave /tb/ahb_if_inst/HREADY
wv.cursors.add -time 531700ps -name {Default cursor}
wv.cursors.setactive -name {Default cursor}
wv.zoom.range -from 484ns -to 620ns
wv.time.unit.auto.set
transcript $curr_transcript
