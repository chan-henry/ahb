`ifndef AHB_MASTER_DRIVER_BFM_SV
`define AHB_MASTER_DRIVER_BFM_SV
interface ahb_master_driver_bfm#(
	ADDR_WIDTH=32,
	DATA_WIDTH=32,
	MAX_BEATS=1024	// AHB spec says bursts can't cross 1KB address boundary
)
(
	ahb_if intf
);
	// Proxy
	import ahb_pkg::*;
	ahb_master_driver_proxy proxy;

	// Transaction
	typedef struct packed{
		logic	[MAX_BEATS-1:0][ADDR_WIDTH-1:0]	HADDR;
		hburst					HBURST;
		logic	[2:0]				HSIZE;
//		htrans					HTRANS;
		logic					HWRITE;
		logic	[MAX_BEATS-1:0][DATA_WIDTH-1:0]	data;
		logic	[$clog2(MAX_BEATS)-1:0]		valid_index;	// For INCR bursts
	} bfm_trans;

	// Local variables
	bfm_trans	trans;

	logic [$clog2(MAX_BEATS)-1:0]	addr_index	= 0;
	logic [$clog2(MAX_BEATS)-1:0]	data_index	= 0;

	logic			trans_toggle0		= 0;
	logic			trans_toggle1		= 0;
	logic			trans_toggle2		= 0;
	logic			write_wdata		= 0;
	logic			read_rdata		= 0;

	logic			send_rsp_trans		= 0;

	// Proxy<->BFM functions
	function void execute_trans(bfm_trans proxy_trans);
		trans		= proxy_trans;
		trans_toggle0	= ~trans_toggle0;
	endfunction

	function logic get_resetn();
		return intf.HRESETn;
	endfunction

	// Reset
	//always@(negedge intf.HRESETn or posedge intf.HRESETn) begin
	//	if(1'b1 == intf.HRESETn) begin
	//		proxy.set_resetn(1);
	//	end
	//
	//	if(1'b0 == intf.HRESETn) begin
	//		proxy.set_resetn(0);
	//	end else begin
	//		proxy.set_resetn(1);
	//	end
	//end

	// Address phase
	always@(posedge intf.HCLK or negedge intf.HRESETn) begin
		if(1'b0 == intf.HRESETn) begin
			intf.HADDR	<= 0;
			intf.HWRITE	<= 0;
			intf.HSIZE	<= 0;
			intf.HBURST	<= hburst'(0);
			intf.HPROT	<= 0;
			intf.HTRANS	<= htrans'(0);
			intf.HMASTLOCK	<= 0;
			intf.HWDATA	<= 0;

			addr_index		<= 0;
			data_index		<= 0;
			trans_toggle0		<= 0;
			trans_toggle1		<= 0;
		end else if(trans_toggle1 != trans_toggle0) begin

			intf.HADDR	<= trans.HADDR[addr_index];
			intf.HWRITE	<= trans.HWRITE;
			intf.HSIZE	<= trans.HSIZE;
			intf.HBURST	<= trans.HBURST;

			// TODO: BFM is currently blocking (does not allow pipelined trans from proxy)
			if(0 == addr_index) begin
				intf.HTRANS <= NONSEQ;
				if(trans.HWRITE) begin
					write_wdata <= 1;
				end else begin
					read_rdata <= 1;
				end
			end else if((SINGLE == trans.HBURST) && (addr_index >= 1)) begin
				intf.HTRANS	<= IDLE;
				write_wdata	<= 0;
				read_rdata	<= 0;
			end else if((WRAP4 == trans.HBURST || INCR4 == trans.HBURST) && (addr_index >= 4)) begin
				intf.HTRANS	<= IDLE;
				write_wdata	<= 0;
				read_rdata	<= 0;
			end else if((WRAP8 == trans.HBURST || INCR8 == trans.HBURST) && (addr_index >= 8)) begin
				intf.HTRANS	<= IDLE;
				write_wdata	<= 0;
				read_rdata	<= 0;
			end else if((WRAP16 == trans.HBURST || INCR16 == trans.HBURST) && (addr_index >= 16)) begin
				intf.HTRANS	<= IDLE;
				write_wdata	<= 0;
				read_rdata	<= 0;
			end else if((INCR == trans.HBURST) && (addr_index >= trans.valid_index + 1)) begin
				intf.HTRANS	<= IDLE;
				write_wdata	<= 0;
				read_rdata	<= 0;
			end else begin
				intf.HTRANS <= SEQ;
			end

			if(intf.HREADY) begin
				if(SINGLE == trans.HBURST) begin
					if(addr_index >= 1) begin
						addr_index		<= 0;
						trans_toggle1		<= trans_toggle0;
					end else begin
						addr_index		<= addr_index + 1;
					end
				end else if(WRAP4 == trans.HBURST || INCR4 == trans.HBURST) begin
					if(addr_index >= 4) begin
						addr_index		<= 0;
						trans_toggle1		<= trans_toggle0;
					end else begin
						addr_index		<= addr_index + 1;
					end
				end else if(WRAP8 == trans.HBURST || INCR8 == trans.HBURST) begin
					if(addr_index >= 8) begin
						addr_index		<= 0;
						trans_toggle1		<= trans_toggle0;
					end else begin
						addr_index		<= addr_index + 1;
					end
				end else if(WRAP16 == trans.HBURST || INCR16 == trans.HBURST) begin
					if(addr_index >= 16) begin
						addr_index		<= 0;
						trans_toggle1		<= trans_toggle0;
					end else begin
						addr_index		<= addr_index + 1;
					end
				end else if(INCR == trans.HBURST) begin
					if(addr_index >= trans.valid_index + 1) begin
						addr_index		<= 0;
						trans_toggle1		<= trans_toggle0;
					end else begin
						addr_index		<= addr_index + 1;
					end
				end
			end
		end
	end

	// Data phase
	always@(posedge intf.HCLK or negedge intf.HRESETn) begin
		if(1'b0 == intf.HRESETn) begin
			send_rsp_trans <= 0;
		end else if(write_wdata || read_rdata || (trans_toggle0 == trans_toggle1 && trans_toggle0 != trans_toggle2))begin
			if(write_wdata) begin
				intf.HWDATA	<= trans.data[data_index];
			end

			if(read_rdata) begin
				trans.data[data_index] <= intf.HRDATA;
			end

			if(intf.HREADY) begin
				if(SINGLE == trans.HBURST) begin
					if(data_index >= 1) begin
						data_index		<= 0;
					end else begin
						data_index		<= data_index + 1;
					end
				end else if(WRAP4 == trans.HBURST || INCR4 == trans.HBURST) begin
					if(data_index >= 4) begin
						data_index		<= 0;
					end else begin
						data_index		<= data_index + 1;
					end
				end else if(WRAP8 == trans.HBURST || INCR8 == trans.HBURST) begin
					if(data_index >= 8) begin
						data_index		<= 0;
					end else begin
						data_index		<= data_index + 1;
					end
				end else if(WRAP16 == trans.HBURST || INCR16 == trans.HBURST) begin
					if(data_index >= 16) begin
						data_index		<= 0;
					end else begin
						data_index		<= data_index + 1;
					end
				end else if(INCR == trans.HBURST) begin
					if(data_index >= trans.valid_index + 1) begin
						data_index		<= 0;
					end else begin
						data_index		<= data_index + 1;
					end
				end
			end
		end
	end
	always@(posedge intf.HCLK or negedge intf.HRESETn) begin
		if(1'b0 == intf.HRESETn) begin
			send_rsp_trans <= 0;
		end else if((trans_toggle2 != trans_toggle0) && intf.HREADY) begin
			if(SINGLE == trans.HBURST) begin
				if(data_index >= 1) begin
					trans_toggle2	<= trans_toggle0;
					send_rsp_trans		<= 1;
				end else begin
					send_rsp_trans	<= 0;
				end
			end else if(WRAP4 == trans.HBURST || INCR4 == trans.HBURST) begin
				if(data_index >= 4) begin
					trans_toggle2	<= trans_toggle0;
					send_rsp_trans		<= 1;
				end else begin
					send_rsp_trans		<= 0;
				end
			end else if(WRAP8 == trans.HBURST || INCR8 == trans.HBURST) begin
				if(data_index >= 8) begin
					trans_toggle2	<= trans_toggle0;
					send_rsp_trans		<= 1;
				end else begin
					send_rsp_trans		<= 0;
				end
			end else if(WRAP16 == trans.HBURST || INCR16 == trans.HBURST) begin
				if(data_index >= 16) begin
					trans_toggle2	<= trans_toggle0;
					send_rsp_trans		<= 1;
				end else begin
					send_rsp_trans		<= 0;
				end
			end else if(INCR == trans.HBURST) begin
				if(data_index >= trans.valid_index + 1) begin
					trans_toggle2	<= trans_toggle0;
					send_rsp_trans		<= 1;
				end else begin
					send_rsp_trans		<= 0;
				end
			end else begin
				send_rsp_trans <= 0;
			end
		end else begin
			send_rsp_trans <= 0;
		end

		if(send_rsp_trans) begin
			proxy.set_rsp_trans(trans);
		end
	end

endinterface
`endif
