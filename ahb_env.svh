`ifndef AHB_ENV_SVH
`define AHB_ENV_SVH
class ahb_env extends uvm_env;

	`uvm_component_utils(ahb_env)

	ahb_agent	ahb_agent_inst;

	function new(string name, uvm_component parent);
		super.new(name, parent);
	endfunction: new

	function void build_phase(uvm_phase phase);
		super.build_phase(phase);

		ahb_agent_inst	= ahb_agent::type_id::create("ahb_agent_inst", this);
	endfunction: build_phase

	function void connect_phase(uvm_phase phase);
		super.connect_phase(phase);
		//ahb_agent_inst.ap.connect(ahb_scoreboard_inst.axp_in);
	endfunction: connect_phase

endclass
`endif
