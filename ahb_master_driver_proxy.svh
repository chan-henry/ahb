`ifndef AHB_MASTER_DRIVER_PROXY_SV
`define AHB_MASTER_DRIVER_PROXY_SV
class ahb_master_driver_proxy#(
	ADDR_WIDTH=32,
	DATA_WIDTH=32,
	MAX_BEATS=1024
) extends uvm_driver#(ahb_transaction);

	`uvm_component_utils(ahb_master_driver_proxy)

	// BFM
	virtual ahb_master_driver_bfm bfm;

	// Transaction
	typedef struct packed{
		logic	[MAX_BEATS-1:0][ADDR_WIDTH-1:0]	HADDR;
		hburst					HBURST;
		logic	[2:0]				HSIZE;
//		htrans					HTRANS;
		logic					HWRITE;
		logic	[MAX_BEATS-1:0][DATA_WIDTH-1:0]	data;
		logic	[$clog2(MAX_BEATS)-1:0]		valid_index;	// For INCR bursts
	} bfm_trans;

	// Local variables
	bfm_trans	req_trans;
	bfm_trans	rsp_trans;
	logic		rsp_done = 0;
	logic		resetn = 0;

	// Proxy<->BFM functions
	function void set_rsp_trans(bfm_trans trans);
		rsp_trans	= trans;
		rsp_done	= 1;
	endfunction

	function void set_resetn(logic val);
		resetn = val;
	endfunction

	function bfm_trans cnvt_to_bfm_trans(ahb_transaction#(ADDR_WIDTH, DATA_WIDTH, MAX_BEATS) trans);
		bfm_trans temp_trans;

		temp_trans.HADDR	= trans.HADDR;
		temp_trans.HBURST	= hburst'(trans.HBURST);
		temp_trans.HSIZE	= trans.HSIZE;
		temp_trans.HWRITE	= trans.HWRITE;
		temp_trans.data		= trans.data;
		temp_trans.valid_index	= trans.valid_index;

		return temp_trans;
	endfunction

	function new(string name, uvm_component parent);
		super.new(name, parent);
	endfunction

	function void build_phase(uvm_phase phase);
		super.build_phase(phase);

		if(!uvm_config_db#(virtual ahb_master_driver_bfm)::get	(
									.cntxt(this),
									.inst_name(""),
									.field_name("bfm"),
									.value(bfm)
									)
		) begin
			`uvm_fatal(get_full_name(), "Failed uvm_config_db get of bfm")
		end
		bfm.proxy = this;
	endfunction

	task run_phase(uvm_phase phase);
		ahb_transaction#(ADDR_WIDTH, DATA_WIDTH, MAX_BEATS) trans;
		int count = 0;

		forever begin
			//if(1'b1 == resetn) begin
				seq_item_port.get_next_item(trans);
				count++;
				`uvm_info(get_full_name(), $sformatf("Got trans in proxy, count: %0d", count), UVM_LOW)

				req_trans = cnvt_to_bfm_trans(trans);
				bfm.execute_trans(req_trans);
				wait(rsp_done);
				rsp_done = 0;
				seq_item_port.item_done();
			//end
		end
	endtask
endclass
`endif // def AHB_MASTER_DRIVER_PROXY_SV
