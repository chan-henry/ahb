onerror { quit }

vlib ahb

vlog				\
-work ahb			\
+incdir+"$env(UVM_SRC)"+.	\
-l uvm_1_2			\
-coverage sbeca			\
-error_limit 1			\
-msg 1				\
ahb_pkg.sv			\
top.sv

#do sim.do
