#####################################
# Interesting seed values:
# - #####
#####################################

vsim			\
-sv_seed random		\
+UVM_TESTNAME=ahb_test	\
+access +r		\
-acdb			\
ahb.tb

log -rec *

run -all

endsim

#do cov_report.do

#acdb report			\
#-html				\
#-i ahb.acdb			\
#-o coverage_report.html

quit
