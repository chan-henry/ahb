`ifndef AHB_AGENT_SVH
`define AHB_AGENT_SVH
class ahb_agent extends uvm_agent; 

	`uvm_component_utils(ahb_agent)

	uvm_analysis_port#(ahb_transaction) ap;

	ahb_master_driver_proxy	ahb_driver_inst;
	//ahb_monitor	ahb_monitor_inst;
	ahb_sequencer	ahb_sequencer_inst;

	function new(string name, uvm_component parent);
		super.new(name, parent);
	endfunction: new

	function void build_phase(uvm_phase phase);
		super.build_phase(phase);

		ahb_driver_inst		= ahb_master_driver_proxy#(32,32,1024)::type_id::create("ahb_driver_inst", this);
		//ahb_monitor_inst	= ahb_monitor::type_id::create("ahb_monitor_inst", this);
		ahb_sequencer_inst	= ahb_sequencer::type_id::create("ahb_sequencer_inst", this);

		ap = new(.name("ap"),.parent(this));
	endfunction: build_phase

	function void connect_phase(uvm_phase phase);
		super.connect_phase(phase);

		ahb_driver_inst.seq_item_port.connect(ahb_sequencer_inst.seq_item_export);
		uvm_report_info(get_name(), ": connect_phase, driver and sequencer connected");

		//ahb_monitor_inst.ap.connect(ap);
	endfunction: connect_phase

endclass
`endif
