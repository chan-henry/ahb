`ifndef AHB_TEST_SVH
`define AHB_TEST_SVH
class ahb_test extends uvm_test;

	`uvm_component_utils(ahb_test)

	ahb_env		ahb_env_inst;

	function new(string name, uvm_component parent);
		super.new(name, parent);
	endfunction: new

	function void build_phase(uvm_phase phase);
		super.build_phase(phase);

		ahb_env_inst	= ahb_env::type_id::create(.name("ahb_env_inst"), .parent(this));
	endfunction: build_phase

	task run_phase(uvm_phase phase);
		// Start sequences here
		ahb_rand_sequence	my_ahb_seq;

		phase.raise_objection(.obj(this),.description(get_name()));

		#500ns;	// FIXME: This is temporary until clk/rst gen is made into xtors

		my_ahb_seq = ahb_rand_sequence::type_id::create("my_ahb_seq");
		my_ahb_seq.count = 200;
		my_ahb_seq.start(ahb_env_inst.ahb_agent_inst.ahb_sequencer_inst);

		//phase.phase_done.set_drain_time(this, 8000000ns);	// Is there a more elegant solution to ending the sim?
		phase.phase_done.set_drain_time(this, 100);	// Is there a more elegant solution to ending the sim?
		
		phase.drop_objection(this);
		`uvm_info(get_full_name(), "Dropped objection", UVM_LOW)
	endtask: run_phase

endclass: ahb_test
`endif
